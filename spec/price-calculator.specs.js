describe('price calculator', () => {

	describe('User role', () => {
		it('should return company user role if userType is 1', () => {
			const userType = 1;
			const userRole = getUserRole(userType);

			expect(userRole).to.equal('company');
			expect(userRole).not.equal('normal');
		});

		it('should return normal user role if userType is 0', () => {
			const userType = 0;
			const userRole = getUserRole(userType);

			expect(userRole).to.equal('normal');
			expect(userRole).not.equal('company');
		});
	});

	describe('Product type', () => {
		it('should return new tag if productType is 0', () => {
			const productType = 0;
			const productTag = getProductTag(productType);

			expect(productTag).to.equal('new');
			expect(productTag).not.equal('old');
		});

		it('should return old tag if productType is 1', () => {
			const productType = 1;
			const productTag = getProductTag(productType);

			expect(productTag).to.equal('old');
			expect(productTag).not.equal('new');
		});
	});

	describe('Additional price calculation', () => {
		it('should have 25 additional price if product is new', () => {
			const productType = 'new';
			const additionalPrice = calculateAdditionalPrice(productType);

			expect(additionalPrice).to.equal(25);
		});

		it('should have 35 additional price if product is old', () => {
			const productType = 'old';
			const additionalPrice = calculateAdditionalPrice(productType);

			expect(additionalPrice).to.equal(35);
		});
	});

	describe('Rebate calculation', () => {
		it('should check if product is published today', () => {
			const publishedDate = new Date();
			const actual = isPublishedToday(publishedDate);

			expect(actual).to.equal(true);
		});

		it('should have 10 discount if user is normal AND product is new AND published today ', () => {
			const userType = 'normal';
			const productType = 'new';
			const publishedDate = new Date();
			const expected = 10;
			const actual = calculateRebate(userType, publishedDate, productType);

			expect(expected).to.equal(actual);
		});

		it('should have 0 discount if user is normal AND product is old', () => {
			const userType = 'normal';
			const productType = 'old';
			const publishedDate = new Date();
			const expected = 0;
			const actual = calculateRebate(userType, publishedDate, productType);

			expect(expected).to.equal(actual);
		});

		it('should have 15 discount if user is company AND product is new AND published today', () => {
			const userType = 'company';
			const productType = 'new';
			const publishedDate = new Date();
			const expected = 15;
			const actual = calculateRebate(userType, publishedDate, productType);

			expect(expected).to.equal(actual);
		});

		it('should have 0 discount if user is company AND product is old', () => {
			const userType = 'company';
			const productType = 'old';
			const publishedDate = new Date();
			const expected = 5;
			const actual = calculateRebate(userType, publishedDate, productType);

			expect(expected).to.equal(actual);
		});
	});

	describe('Final price calcuation', () => {
		it('should calc price right if user is normal, product is new and published today', () => {
			const userType = 0; //normal user
			const productType = 0; //new product
			const price = 1; //productPrice
			const publishedDate = new Date();
			const expected = 16;
			const actual = calculatePrice(userType, productType, price, publishedDate);

			expect(expected).to.equal(actual);
		});

		it('should calc price right if user is normal, product is new BUT published yesterday', () => {
			const userType = 0; //normal user
			const productType = 0; //new product
			const price = 1; //productPrice
			const publishedDate = new Date('yesterday');
			const expected = 26;
			const actual = calculatePrice(userType, productType, price, publishedDate);

			expect(expected).to.equal(actual);
		});

		it('should calc price right if user is company, product is new BUT published yesterday', () => {
			const userType = 1; //company user
			const productType = 0; //new product
			const price = 1; //productPrice
			const publishedDate = new Date('yesterday');
			const expected = 21;
			const actual = calculatePrice(userType, productType, price, publishedDate);

			expect(expected).to.equal(actual);
		});

		it('should calc price right if user is company, product is old', () => {
			const userType = 1; //company user
			const productType = 1; //old product
			const price = 1; //productPrice
			const publishedDate = new Date();
			const expected = 31;
			const actual = calculatePrice(userType, productType, price, publishedDate);

			expect(expected).to.equal(actual);
		});
	});

});
