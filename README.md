# Design motivation #

For the given refactored task, I have choose to create small and abstracts pure functions.

### Benefits of using functional design approch ###

* Pure functions are easier to reason about. (Just Input and Output)
* Testing is easier.
* Debugging is easier.
* Easier to maintain / scale.
* No side effects.
* Programs are more bulletproof.

### Unit Tests
![Unit tests](https://preview.ibb.co/mtb5uy/Screen_Shot_2018_06_30_at_11_04_19.png)

### Further improvements:
* Recommanded to use TypeScript to strictly define types of input params and output.
* For growing application, it is good to use webpack to dynamically *load, concat and minify* js file before deploying to production.
