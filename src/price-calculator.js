"use strict";

// userType, 0 = normal, 1 = company
function getUserRole(userType) {
	return (userType === 1) ? 'company' : 'normal';
}

// productType, 0 = new product, 1 = old product
function getProductTag(productType) {
	return (productType === 0) ? 'new' : 'old'
}

// check if product is published today?
function isPublishedToday(publishedDate) {
	return publishedDate.toDateString() === new Date().toDateString();
}

// calculate product rebate based on following rules
// Rule:1 If the product is newand is published todayyou will receive 10 SEK in rebate
// Rule:2 If you are a company user, you will receive 5 SEK in rebate
function calculateRebate(userType, publishedDate, productTag) {
	const newProductDiscount = (productTag === 'new' && isPublishedToday(publishedDate)) ? 10 : 0;
	return (userType === 'company') ? (newProductDiscount + 5) : newProductDiscount;
}

// calculate additional price based on productTag
function calculateAdditionalPrice(productTag) {
	return (productTag === 'new') ? 25 : 35;
}

// calculate final price
function calculatePrice(userType, productType, productPrice, publishedDate) {
	const userRoleName = getUserRole(userType);
	const productTag = getProductTag(productType);
	const additionalPrice = calculateAdditionalPrice(productTag);
	const productRebate = calculateRebate(userRoleName, publishedDate, productTag);

	return (productPrice + additionalPrice) - productRebate;
}
